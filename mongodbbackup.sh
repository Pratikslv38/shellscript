#!/bin/bash

export PATH=/bin:/usr/bin:/usr/local/bin
TODAY=`date "+%Y-%m-%d-%H:%M:%S"`

DB_BACKUP_PATH='/home/ubuntu/backup'
MONGO_HOST='127.0.0.1'
MONGO_PORT='27017'
MONGO_USER='myTester'
MONGO_PASSWD='xyz123'
DATABASE_NAMES='$1'

mkdir -p ${DB_BACKUP_PATH}/${TODAY}

echo "Running backup for selected databases"
for DB_NAME in ${DATABASE_NAMES}
do
mongodump --host ${MONGO_HOST} --port ${MONGO_PORT} --db $1 ${AUTH_PARAM} --out ${DB_BACKUP_PATH}/${TODAY}/
done

=============MONGO ATLAS DB BACKUP SCRIPT=================
#!/bin/bash
export PATH=/bin:/usr/bin:/usr/local/bin
TODAY=`date "+%Y-%m-%d-%H:%M:%S"`

DB_BACKUP_PATH='/home/ubuntu/backup'
MONGO_HOST='127.0.0.1'
MONGO_PORT='27017'
MONGO_USER='MyDB'
MONGO_PASSWD='xyz123'
DATABASE_NAMES='$1'

mkdir -p ${DB_BACKUP_PATH}/${TODAY}

echo "Running backup for selected databases"
for DB_NAME in ${DATABASE_NAMES}
do
        mongodump --uri mongodb+srv://MyDB:xyz123@cluster0.uqleiiq.mongodb.net/ --db $1 --out ${DB_BACKUP_PATH}/${TODAY}/
done

