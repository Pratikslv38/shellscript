#!/bin/bash

export PATH=/bin:/usr/bin:/usr/local/bin
TIMESTAMP= `date "+%Y-%m-%d-%H:%M:%S"`

DB_BACKUP_PATH_Dev='/home/ubuntu/backup/Dev/'
DB_BACKUP_PATH_Prod='/home/ubuntu/backup/Prod'
DB_BACKUP_PATH_Stage='/home/ubuntu/backup/Stage'
DB_BACKUP_PATH_UAT='/home/ubuntu/backup/UAT'
MONGO_HOST='127.0.0.1'
MONGO_PORT='27017'
MONGO_USER='MyDB'
MONGO_PASSWD='xyz123'
DATABASE_NAMES='$1'

echo "Restoring backup for selected database"
case $1 in
        Dev)
         `mongorestore --uri mongodb+srv://MyDB:xyz123@cluster0.uqleiiq.mongodb.net/ ${DB_BACKUP_PATH_Dev}/${TIMESTAMP}`
         ;;
        Prod)
         `mongorestore --uri mongodb+srv://MyDB:xyz123@cluster0.uqleiiq.mongodb.net/ ${DB_BACKUP_PATH_Dev}/${TIMESTAMP}`
         ;;
        Stage)
         `mongorestore --uri mongodb+srv://MyDB:xyz123@cluster0.uqleiiq.mongodb.net/ ${DB_BACKUP_PATH_Dev}/${TIMESTAMP}`
         ;;
        UAT)
         `mongorestore --uri mongodb+srv://MyDB:xyz123@cluster0.uqleiiq.mongodb.net/ ${DB_BACKUP_PATH_Dev}/${TIMESTAMP}`
         ;;
esac

