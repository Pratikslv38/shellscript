#!/bin/bash

export PATH=/bin:/usr/bin:/usr/local/bin
TODAY=`date "+%Y-%m-%d-%H:%M:%S"`

DB_BACKUP_PATH_Dev='/home/ubuntu/backup/Dev'
DB_BACKUP_PATH_UAT='/home/ubuntu/backup/UAT'
DB_BACKUP_PATH_Stage='/home/ubuntu/backup/Stage'
DB_BACKUP_PATH_Prod='/home/ubuntu/backup/Prod'
MONGO_HOST='127.0.0.1'
MONGO_PORT='27017'
MONGO_USER='MyDB'
MONGO_PASSWD='xyz123'
DATABASE_NAMES='$1'

echo "Running backup for selected databases"

case $1 in

        Dev)
        `mongodump --uri mongodb+srv://MyDB:xyz123@cluster0.uqleiiq.mongodb.net/ --db Dev --out ${DB_BACKUP_PATH_Dev}/${TODAY}`
        ;;
        Prod)
        `mongodump --uri mongodb+srv://MyDB:xyz123@cluster0.uqleiiq.mongodb.net/ --db Prod --out ${DB_BACKUP_PATH_Prod}/${TODAY}`
        ;;
        UAT)
        `mongodump --uri mongodb+srv://MyDB:xyz123@cluster0.uqleiiq.mongodb.net/ --db UAT --out ${DB_BACKUP_PATH_UAT}/${TODAY}`
        ;;
        Stage)
        `mongodump --uri mongodb+srv://MyDB:xyz123@cluster0.uqleiiq.mongodb.net/ --db Stage --out ${DB_BACKUP_PATH_Stage}/${TODAY}`
        ;;
esac

