#!/bin/bash

read -p "Enter the server name: " web_server
status=$(systemctl status $web_server | awk 'NR==3 { print $2 }')
echo -e "The server status $web_server is: $status"
