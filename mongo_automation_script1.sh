#!/bin/bash
      if [ ! -f /usr/bin/mongod ]
      then
      sudo curl -fsSL https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
      sudo echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
      sudo apt update
      sudo apt install mongodb-org -y
      sudo systemctl start mongod.service
      sudo sed -i "s,\\(^[[:blank:]]*bindIp:\\) .*,\\1 0.0.0.0," /etc/mongod.conf
      sudo systemctl restart mongod
else
      echo "mongo db already installed.  Skipping..."
fi


      echo "Enter Database Name"
      read db
      mongo --eval 'db.getMongo().getDBNames()' | grep -w $db
      if [ $? == 0 ]; then
      echo "MongoDB database  exists"
      else
      echo "MongoDB database does not exist"
      mongo $db --eval 'db.createCollection("first"); db.createCollection("second"); db.createCollection("third")'
      fi

