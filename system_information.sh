#!/bin/sh
show_menu(){
    
    printf "\n${menu}*********************************************${normal}\n"
    printf "${menu}**${number} 1)${menu} System Configuration ${normal}\n"
    printf "${menu}**${number} 2)${menu} kernel Release ${normal}\n"
    printf "${menu}**${number} 3)${menu} Bash Version ${normal}\n"
    printf "${menu}**${number} 4)${menu} Restart Apache ${normal}\n"
    printf "${menu}**${number} 5)${menu} Create File ${normal}\n"
    printf "${menu}**${number} 6)${menu} Free Space ${normal}\n"
    printf "${menu}**${number} 7)${menu} Free memory ${normal}\n"
    printf "${menu}**${number} 8)${menu} Virtual mem stat ${normal}\n"
    printf "${menu}**${number} 9)${menu} CPU and I/O Statistics ${normal}\n"
    printf "${menu}**${number} 10)${menu} Username ${normal}\n"
    printf "${menu}**${number} 11)${menu} CPU Usage ${normal}\n"
    printf "${menu}**${number} 12)${menu} Status apache2 ${normal}\n"
    printf "${menu}**${number} 13)${menu} ifconfig ${normal}\n"
    printf "${menu}**${number} 14)${menu} Create script file ${normal}\n"
    printf "${menu}**${number} 15)${menu} Current Date & Time ${normal}\n"
    printf "${menu}**${number} 16)${menu} Calendar ${normal}\n"
    printf "${menu}**${number} 17)${menu} Current Shell ${normal}\n"
    printf "${menu}*********************************************${normal}\n"
    printf "Please enter a menu option and enter or ${fgred}x to exit. ${normal}"
    read opt
}

option_picked(){
    msgcolor=`echo "\033[01;31m"` # bold red
    normal=`echo "\033[00;00m"` # normal white
    message=${@:-"${normal}Error: No message passed"}
    printf "${msgcolor}${message}${normal}\n"
}

freespace=$(df -h / | awk 'NR==2 {print $4}')
freemem=$(free -h |awk '{print$3}' | sed '3d')
vmstat=$(vmstat -s | head)
loadaverage=$(num_iterations=3 | top -b -n $num_iterations | head -5)
iostat=$(iostat | head -4 | sed 1d)
user=$USER
apache2=$(systemctl status apache2 | grep "active" )


clear
show_menu
while [ $opt != '' ]
    do
    if [ $opt = '' ]; then
      exit;
    else
      case $opt in
	1) clear;
            option_picked "Option 1 Picked";
            printf "%s\n" "$(lsb_release -a)";
            show_menu;
        ;;	    
        2) clear;
            option_picked "Option 2 Picked";
            printf "\tKernel Release:\t%s\n" $(uname -r);
            show_menu;
        ;;
        3) clear;
            option_picked "Option 3 Picked";
	    printf "\tBash Version:\t%s\n" $BASH_VERSION;
            show_menu;
        ;;
        4) clear;
            option_picked "Option 4 Picked";
            printf "sudo service apache2 restart";
            show_menu;
        ;;
        5) clear;
            option_picked "Option 5 Picked";
            printf "\tFile Created:\t%s\n" $(touch > pratik.txt /home/pratik/Desktop/test )
            show_menu;
        ;;
        6) clear;
	    option_picked "Option 6 Picked";
	    printf "\tFree Storage:\t%s\n" $freespace;
	    show_menu;
	;;
        7) clear;
	    option_picked "Option 7 Picked";
	    printf "\tFree Memory:\t%s\n" $freemem;
	    show_menu;
	;;
        8) clear;
            option_picked "Option 8 Picked";
	    printf "\t%b\n\t" "$(vmstat)"
            show_menu;
	;;
        9) clear;
            option_picked "Option 9 Picked";
            printf "\t\vCPU and I/O Statistics:\t\v%s\n" $iostat;
            show_menu;
	;;
	10) clear;
            option_picked "Option 10 Picked";
	    printf "\tUsername:\t%s\n" $user;
            show_menu;
	;;
        11) clear;
            option_picked "Option 11 Picked";
            printf "\tCPU Usage:\t%s\n" $loadaverage;
            show_menu;
	;;
	12) clear;
            option_picked "Option 12 Picked";
	    printf "%s\n" "$(systemctl status apache2 | grep "active" )";
            show_menu;
	;;
	13) clear;
            option_picked "Option 13 Picked";
	    printf "$(ifconfig | grep "inet" | awk '{print $1 $2}' | sed -n 1p)"
            show_menu;
	;;
	14) clear;
            option_picked "Option 14 Picked";
	    printf "\tCreate script file:\t%s\n" $(touch > pratik.sh /home/pratik/Desktop/test);
            show_menu;
	;;
	15) clear;
            option_picked "Option 15 Picked";
	    printf "%b" "\n" "$(date)";
            show_menu;
	;;
	16) clear;
            option_picked "Option 16 Picked";
            printf "%b" "\n" "$(cal)";
            show_menu;
	;;
        17) clear;
            option_picked "Option 17 Picked";
            printf "The current shell is: "$SHELL
            show_menu;
	;;
        x)exit;
        ;;
        \n)exit;
        ;;
        *)clear;
            option_picked "Pick an option from the menu";
            show_menu;
        ;;
      esac
    fi
done
