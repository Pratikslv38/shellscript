#!/bin/bash

CET () {
  local PS3='Please enter country name: '
  local options=("Algiers" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Algiers")
           echo "The current timezone of Algiers: "
           TZ=GMT-01:00 date;TZ=IST-5:30 date
              ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}


EAT () {
  local PS3='Please enter country name: '
  local options=("Addis_Ababa" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
           "Addis_Ababa")
           echo "The current timezone of Addis_Ababa: "
           TZ=GMT-03:00 date;TZ=IST-5:30 date
              ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}


GMT () {
  local PS3='Please enter country name: '
  local options=("Abidjan" "Accra" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Abidjan")
           echo "The current timezone of Abidjan: "
           export TZ=Abidjan;date;TZ=Asia/Kolkata;date
              ;;
          "Accra")
           echo "The current timezone of Accra: "
           export TZ=Accra;date;TZ=Asia/Kolkata;date
              ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

AST () {
  local PS3='Please enter country name: '
  local options=("Antigua" "Barbados" "Dominica" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Antigua")
           echo "The current timezone of Antigua: "
           TZ=GMT+04:00 date;TZ=IST-5:30 date
              ;;
          "Barbados")
           echo "The current timezone of Barbados: "
           TZ=GMT+04:00 date;TZ=IST-5:30 date
              ;;
	  "Dominica")
           echo "The current timezone of Dominica: "
           TZ=GMT+04:00 date;TZ=IST-5:30 date
              ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

CST () {
  local PS3='Please enter country name: '
  local options=("El_Salvador" "Costa_Rica" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "El_Salvador")
           echo "The current timezone of El_Salvador: "
           TZ=GMT+06:00 date;TZ=IST-5:30 date
              ;;
          "Costa_Rica")
           echo "The current timezone of Costa_Rica: "
           TZ=GMT+04:00 date;TZ=IST-5:30 date
              ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

EDT () {
  local PS3='Please enter country name: '
  local options=("Toronto" "New_York" "Indianapolis" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Toronto")
           echo "The current timezone of Toronto: "
           TZ=GMT+04:00 date;TZ=IST-5:30 date
              ;;
          "New_York")
           echo "The current timezone of New_York: "
           TZ=GMT+04:00 date;TZ=IST-5:30 date
              ;;
          "Indianapolis")
           echo "The current timezone of Indianapolis: "
           TZ=GMT+04:00 date;TZ=IST-5:30 date
	   ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

PDT () {
  local PS3='Please enter country name: '
  local options=("Los_Angeles" "Tijuana" "Vancouver" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Los_Angeles")
           echo "The current timezone of Los_Angeles: "
           TZ=GMT-07:00 date;TZ=IST-5:30 date
              ;;
          "Tijuana")
           echo "The current timezone of Tijuana: "
           TZ=GMT-07:00 date;TZ=IST-5:30 date
              ;;
          "Vancouver")
           echo "The current timezone of Vancouver: "
           TZ=GMT-07:00 date;TZ=IST-5:30 date
	   ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

CDT () {
  local PS3='Please enter country name: '
  local options=("Mexico_City" "Tijuana" "Vancouver" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Mexico_City")
           echo "The current timezone of Mexico_city: "
           TZ=GMT+05:00 date;TZ=IST-5:30 date
              ;;
          "Winnipeg")
           echo "The current timezone of Winnipeg: "
           TZ=GMT+05:00 date;TZ=IST-5:30 date
              ;;
          "Monterrey")
           echo "The current timezone of Monterrey: "
           TZ=GMT+05:00 date;TZ=IST-5:30 date
	   ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

ICT () {
  local PS3='Please enter country name: '
  local options=("Bangkok" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Bangkok")
           echo "The current timezone of Bangkok: "
           TZ=GMT-07:00 date;TZ=IST-5:30 date
           ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

HKT () {
  local PS3='Please enter country name: '
  local options=("Hong_kong" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Hong_kong")
           echo "The current timezone of Hong_kong: "
           TZ=GMT-08:00 date;TZ=IST-5:30 date
           ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

BDST () {
  local PS3='Please enter country name: '
  local options=("Dhaka" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Dhaka")
           echo "The current timezone of Dhaka: "
           TZ=GMT-07:00 date;TZ=IST-5:30 date
           ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

SGT () {
  local PS3='Please enter country name: '
  local options=("Singapore" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Singapore")
           echo "The current timezone of Singapore: "
           TZ=GMT-08:00 date;TZ=IST-5:30 date
           ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}


AST () {
  local PS3='Please enter country name: '
  local options=("Baghdad" "Bahrain" "Kuwait" "Qatar" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Baghdad")
           echo "The current timezone of Baghdad: "
           TZ=GMT-03:00 date;TZ=IST-5:30 date
              ;;
          "Bahrain")
           echo "The current timezone of Bahrain: "
           TZ=GMT-03:00 date;TZ=IST-5:30 date
              ;;
          "Kuwait")
           echo "The current timezone of Kuwait: "
           TZ=GMT-03:00 date;TZ=IST-5:30 date
              ;;
	  "Qatar")
           echo "The current timezone of Qatar: "
           TZ=GMT-03:00 date;TZ=IST-5:30 date
	   ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

BST () {
  local PS3='Please enter country name: '
  local options=("London" "Guernsey" "Isle_of_Man" "Jersey" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "London")
           echo "The current timezone of London: "
           TZ=GMT-01:00 date;TZ=IST-5:30 date
              ;;
          "Guernsey")
           echo "The current timezone of Guernsey: "
           TZ=GMT-01:00 date;TZ=IST-5:30 date
              ;;
          "Isle_of_Man")
           echo "The current timezone of Helsinki: "
           TZ=GMT-01:00 date;TZ=IST-5:30 date
              ;;
          "Jersey")
           echo "The current timezone of Jersey: "
           TZ=GMT-01:00 date;TZ=IST-5:30 date
           ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

Australia () {
  local PS3='Please enter country name: '
  local options=("Canberra" "Queensland" "Tasmania" "Victoria" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Canberra")
           echo "The current timezone of Canberra: "
           TZ=GMT-10:00 date;TZ=IST-5:30 date
              ;;
          "Queensland")
           echo "The current timezone of Queensland: "
           TZ=GMT-10:00 date;TZ=IST-5:30 date
              ;;
          "Tasmania")
           echo "The current timezone of Tasmania: "
           TZ=GMT-10:00 date;TZ=IST-5:30 date
              ;;
          "Victoria")
           echo "The current timezone of Victoria: "
           TZ=GMT-10:00 date;TZ=IST-5:30 date
           ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}


ART () {
  local PS3='Please enter country name: '
  local options=("Buenos_Aires" "Catamarca" "Cordoba" "Jujuy" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Buenos_Aires")
           echo "The current timezone of Buenos_Aires: "
           TZ=GMT+03:00 date;TZ=IST-5:30 date
              ;;
          "Catamarca")
           echo "The current timezone of Catamarca: "
           TZ=GMT+03:00 date;TZ=IST-5:30 date
              ;;
          "Cordoba")
           echo "The current timezone of Cordoba: "
           TZ=GMT+03:00 date;TZ=IST-5:30 date
              ;;
          "Jujuy")
           echo "The current timezone of Jujuy: "
           TZ=GMT+03:00 date;TZ=IST-5:30 date
           ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

BRT () {
  local PS3='Please enter country name: '
  local options=("Araguaina" "Bahia" "Belem" "Fortaleza" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Araguaina")
           echo "The current timezone of Araguaina: "
           TZ=GMT+03:00 date;TZ=IST-5:30 date
              ;;
          "Bahia")
           echo "The current timezone of Bahia: "
           TZ=GMT+03:00 date;TZ=IST-5:30 date
              ;;
          "Belem")
           echo "The current timezone of Belem: "
           TZ=GMT+03:00 date;TZ=IST-5:30 date
              ;;
          "Fortaleza")
           echo "The current timezone of Fortaleza: "
           TZ=GMT+03:00 date;TZ=IST-5:30 date
           ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
  esac
done
}

Pacific () {
  local PS3='Please enter country name: '
  local options=("Auckland" "Chatham" "Fakaofo" "Fiji" "Nauru" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Auckland")
           echo "The current timezone of Auckland: "
           TZ=GMT-12:00 date;TZ=IST-5:30 date
              ;;
          "Chatham")
           echo "The current timezone of Chatham: "
           TZ=GMT-12:00 date;TZ=IST-5:30 date
              ;;
          "Fakaofo")
           echo "The current timezone of Fakaofo: "
           TZ=GMT-12:00 date;TZ=IST-5:30 date
              ;;
          "Fiji")
           echo "The current timezone of Fiji: "
           TZ=GMT-12:00 date;TZ=IST-5:30 date
           ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

EEST () {
  local PS3='Please enter country name: '
  local options=("Athens" "Bucharest" "Helsinki" "Mariehamn" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Athens")
           echo "The current timezone of Athens: "
           TZ=GMT-03:00 date;TZ=IST-5:30 date
              ;;
          "Bucharest")
           echo "The current timezone of Bucharest: "
           TZ=GMT-03:00 date;TZ=IST-5:30 date
              ;;
          "Helsinki")
           echo "The current timezone of Helsinki: "
           TZ=GMT-03:00 date;TZ=IST-5:30 date
              ;;
          "Mariehamn")
           echo "The current timezone of Mariehamn: "
           TZ=GMT-03:00 date;TZ=IST-5:30 date
           ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
  esac
done
}

AMT () {
  local PS3='Please enter country name: '
  local options=("Boa_Vista" "Campo_Grande" "Cuiaba" "Manaus" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Boa_Vista")
           echo "The current timezone of Boa_Vista: "
           TZ=GMT+04:00 date;TZ=IST-5:30 date
              ;;
          "Campo_Grande")
           echo "The current timezone of Campo_Grande: "
           TZ=GMT+04:00 date;TZ=IST-5:30 date
              ;;
          "Cuiaba")
           echo "The current timezone of Cuiaba: "
           TZ=GMT+04:00 date;TZ=IST-5:30 date
              ;;
          "Manaus")
           echo "The current timezone of Manaus: "
           TZ=GMT+04:00 date;TZ=IST-5:30 date
           ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

CEST () {
  local PS3='Please enter country name: '
  local options=("Zurich" "Rome" "Copenhagen" "Amsterdam" "Budapest" "Gibraltar" "Madrid" "Monaco" "quit")
  local opt
  select opt in "${options[@]}"
  do
      case $opt in
          "Zurich")
           echo "The current timezone of Zurich: "
           TZ=GMT-02:00 date;TZ=IST-5:30 date
           ;;
	  "Rome")
           echo "The current timezone of Rome: "
           TZ=GMT-02:00 date;TZ=IST-5:30 date
           ;;
           "Copenhagen")
           echo "The current timezone of Copenhagen: "
           TZ=GMT-02:00 date;TZ=IST-5:30 date
           ;;
           "Amsterdam")
           echo "The current timezone of Amsterdam: "
           TZ=GMT-02:00 date;TZ=IST-5:30 date
           ;;
           "Budapest")
           echo "The current timezone of Budapest: "
           TZ=GMT-02:00 date;TZ=IST-5:30 date
           ;;
           "Gibraltar")
           echo "The current timezone of Gibraltar: "
           TZ=GMT-02:00 date;TZ=IST-5:30 date
           ;;
	   "Madrid")
           echo "The current timezone of Madrid: "
           TZ=GMT-02:00 date;TZ=IST-5:30 date
           ;;
           "Monaco")
           echo "The current timezone of Monaco: "
           TZ=GMT-02:00 date;TZ=IST-5:30 date
           ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

Oceania () {
   PS3='Please enter timezone: '
   options=("Australia" "Pacific" "quit")
  select opt in "${options[@]}"
  do
      case $opt in
          "Australia")
             Australia
             ;;
          "Pacific")
             Pacific
            ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}


Europe () {
   PS3='Please enter timezone: '
   options=("CEST" "quit")
  select opt in "${options[@]}"
  do
      case $opt in
          "CEST")
             CEST
             ;;
	  "EEST")
             EEST
	     ;;
           "BST")
	    BST
	    ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

South_America () {
   PS3='Please enter timezone: '
   options=("ART" "BRT" "AMT" "quit")
  select opt in "${options[@]}"
  do
      case $opt in
          "ART")
             ART
              ;;
          "BRT")
             BRT
              ;;
          "AMT")
             AMT
             ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}


North_America () {
   PS3='Please enter timezone: '
   options=("AST" "EAT" "CET" "quit")
  select opt in "${options[@]}"
  do
      case $opt in
          "AST")
             AST
              ;;
          "CST")
             CST
              ;;
          "EDT")
             EDT
              ;;
	  "PDT")
	     PDT
	     ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

Asia () {
   PS3='Please enter timezone: '
   options=("AST" "ICT" "quit")
  select opt in "${options[@]}"
  do
      case $opt in
          "AST")
             AST
              ;;
          "ICT")
             ICT
              ;;
	  "BDST")
	      BDST
	      ;;
	  "HKT")
	     HKT
	     ;;
          "SGT")
	     SGT
	     ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}


Africa () {
   PS3='Please enter timezone: '
   options=("GMT" "EAT" "CET" "quit")
  select opt in "${options[@]}"
  do
      case $opt in
          "GMT")
              GMT
              ;;
	  "EAT")
              EAT
              ;;
          "CET")
              CET
              ;;
          "quit")
              return
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
}

show_menu()
{
       clear
       echo "++++++++++++ MENU +++++++++++++"
       echo "1. Africa"
       echo "2. North America"
       echo "3. Asia"
       echo "4. Europe"
       echo "5. Oceania"
       echo "6. South America"
       echo "7. Exit"
       echo "+++++++++++++++++++++++++++++++"
}

take_input()
{
       local choice
       read -p "Select the continent for timezone: " choice

       case $choice in
               1) Africa ;;
               2) North_America ;;
	       3) Asia ;;
	       4) Europe ;;
	       5) Oceania ;;
	       6) South America ;;
               7) exit 0;;
			   *) echo "Enter Valid Option!!"
                       read -p "Press any key to Continue...."

               esac
}

while true
do
       show_menu
       take_input
done

