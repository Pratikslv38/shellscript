#!/bin/bash

echo "Please select below option: "

echo "Please enter first number: "
read a

echo "Please enter second number: "
read b

if [ $a == $b ]
then
   echo "a is equal to b"
elif [ $a -gt $b ]
then
   echo "a is greater than b"
elif [ $a -lt $b ]
then
   echo "a is less than b"
else
   echo "None of the condition met"
fi
